﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;
using Xamarin.Forms.Maps;



namespace CS_481_HW_5
{
   
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        
       
        public MainPage()
        {
            InitializeComponent();

            //initialize the map location. 
            var initialLocation = MapSpan.FromCenterAndRadius(new Position(33.1307785, -117.1601826), Distance.FromMiles(1));

            map.MoveToRegion(initialLocation);
        }



        //Zoom the map
        void ValueChanged(object sender, ValueChangedEventArgs e)
        {
            double zoomLevel = e.NewValue;
            double Degrees = 360 / (Math.Pow(2, zoomLevel));
            if (map.VisibleRegion != null)
            {
                map.MoveToRegion(new MapSpan(map.VisibleRegion.Center, Degrees, Degrees));
            }
        }

        // Street.Satellite and Hybrid button for the map 
        void OnButtonClicked(object sender, EventArgs e)
        {
            Button locationType = sender as Button;
            switch (locationType.Text)
            {
                case "Street":
                    map.MapType = MapType.Street;
                    break;

                case "Satellite":
                    map.MapType = MapType.Satellite;
                    break;

                case "Hybrid":
                    map.MapType = MapType.Hybrid;
                    break;
            }
        }

        // Make four pins
        void PinClicked(object sender, EventArgs e)
        {


            Pin Sandiego = new Pin
            {
                Position = new Position(32.695855, -117.189502),
                Label = "San diego",
                Address = "Corolado",
                Type = PinType.Place
            };
               

            Pin SouthKorea = new Pin
            {
                Position = new Position(37.428808, 126.863228),
                Label = "SouthKorea",
                Address = "Seoul",
                Type = PinType.Place
            };


            Pin LA = new Pin
            {
                Position = new Position(34.031720, -118.245610),
                Label = "LA",
                Address = "LA",
                Type = PinType.Place
            };

            Pin JejuIsland = new Pin
            {
                Position = new Position(33.397501, 126.546834),
                Label = "JejuIsland",
                Address = "Jeju",
                Type = PinType.Place
            };

         

            map.Pins.Add(Sandiego);
            map.Pins.Add(SouthKorea);
            map.Pins.Add(LA);
            map.Pins.Add(JejuIsland);

           
            
            
        }

        // I tried to move the pin, but I don't know why it is not working. 
        private void PinChange(object sender, System.EventArgs e)
        {

            var input = (Picker)sender;
            int index = input.SelectedIndex;

           

            if(index == 0)
            {              
                map.MoveToRegion (MapSpan.FromCenterAndRadius(new Position(32.695855, -117.189502), Distance.FromMiles(1)));
            }


            else if (index == 1)
            {
                map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(32.695855, -117.189502), Distance.FromMiles(1)));
            }

            else if (index == 2)
            {
                map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(32.695855, -117.189502), Distance.FromMiles(1)));
            }

            else if (index == 3)
            {
               map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(32.695855, -117.189502), Distance.FromMiles(1)));
            }


        }






    }
}
